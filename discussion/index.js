//Express Setup
// 1. Import by using the 'require' directive to get access to the components of the express package or dependency.
const express = require('express')
//2. Use the express() function and assign in to an app variable to create an express app or app server.
const app =express()
//3. Declare a variable for the port of the server
const port = 3000

//Middlewares
//These two .use are essential in express
//Allows your app to read json format data
app.use(express.json())
//Allows your app to read data from forms.
app.use(express.urlencoded({extended: true}))

//Routes 
//GEt request route
app.get('/', (request, response)=> {
	//once the route is accessed it will then send a response "Hello World"
	response.send('Hello World')
})

app.get('/hello',(request, response) =>{
	response.send("Hello from/hello endpoint")
})

// Register user route

// mock database
let users = [];

app.post('/register', (request, response) => {
	if(request.body.username !== " " && request.body.password !== " ") {
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} succeddfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})

// Put request route
app.put('/change-password',(request, response)=>{
	let message

	for (let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			user[i].password == request.body.password
			message = `User ${request.body.username}'s password has been updateed!'`
			break
		}else {
			message =`User does not exist.`
		}
	}
	response.send(message)
})


app.get('/home', (request, response)=> {
	//once the route is accessed it will then send a response "Welcome to Homepage"
	response.send('Welcome to Homepage')
})


app.get('/users', (request, response) => {
		console.log(users)	
})

app.delete('/delete-user',(request, response)=>{
	let message

	for (let a = 0; a < users.length; a++){
		if(request.body.username !== users[a].username){
			message =`User does not exist.`
			break
		}else {
			message = `User ${request.body.username} has been deleted!'`
		}
	}
	response.send(message)
})


app.listen(port, () => console.log(`Server is running at port ${port}`))


